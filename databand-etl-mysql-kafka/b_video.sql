/*
Navicat MySQL Data Transfer

Source Server         : local57
Source Server Version : 50722
Source Host           : localhost:3307
Source Database       : databand

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2020-11-16 15:34:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `databand_video`
-- ----------------------------
DROP TABLE IF EXISTS `databand_video`;
CREATE TABLE `databand_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL COMMENT '标题',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `state` int(11) DEFAULT NULL COMMENT '0-有效，1-无效',
  `modify_time` varchar(512) DEFAULT NULL COMMENT '修改时间',
  `cover_id` varchar(512) DEFAULT NULL COMMENT '专辑id',
  `vid` varchar(512) DEFAULT NULL COMMENT '视频id',
  `url` varchar(512) DEFAULT NULL COMMENT '播放url',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `tx_updatetime` datetime DEFAULT NULL COMMENT '更新时间',
  `c_link_flag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of databand_video
-- ----------------------------
INSERT INTO `databand_video` VALUES ('1', '测试视频1', '1', '2', null, '11235', '34234', 'http://xxxxx.mp4', null, null, null);
