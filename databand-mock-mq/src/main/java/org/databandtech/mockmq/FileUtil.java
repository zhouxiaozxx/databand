package org.databandtech.mockmq;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class FileUtil {

	/**
                  * 修改文件内容：字符串逐行替换
     *
     * @param file：待处理的文件
     * @param oldstr：需要替换的旧字符串
     * @param newStr：用于替换的新字符串
     */
    public static boolean modifyFileContent(File file, String oldstr, String newStr) {
        List<String> list = null;
        try {
            list = FileUtils.readLines(file, "UTF-8");
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).indexOf(oldstr) != -1) {
                    System.out.println(file.getName());
                    System.out.println(list.get(i));
                    String temp = list.get(i).replaceAll(oldstr, newStr);
                    list.remove(i);
                    list.add(i, temp);
                }
            }
            FileUtils.writeLines(file, "UTF-8", list, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

	public static void showFilename(File file) {
		System.out.println(file.getName());
	}

}
