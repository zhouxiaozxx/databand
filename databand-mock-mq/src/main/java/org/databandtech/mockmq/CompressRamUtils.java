package org.databandtech.mockmq;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CompressRamUtils {
	
	private static Logger logger = LoggerFactory.getLogger(CompressRamUtils.class);

	
    public static void main(String[] args) throws Exception {
        Map<String, byte[]> fileBytesMap = null;

        fileBytesMap = new HashMap<String, byte[]>();
        // 设置文件列表.
        File dirFile = new File("C:/Users/Administrator/Downloads/个人文件/2020-07-13/files");
        for (File file : dirFile.listFiles()) {
            fileBytesMap.put(file.getName(), FileUtils.readFileToByteArray(file));
        }

        byte[] ramBytes = CompressRamUtils.compressByTar(fileBytesMap);
        ramBytes = CompressRamUtils.compressByGZip(ramBytes);
        FileUtils.writeByteArrayToFile(new File("C:/Users/Administrator/Downloads/个人文件/2020-07-13/ram.tar.gz"), ramBytes);
        
        ramBytes = CompressRamUtils.decompressByGZip(ramBytes);
        fileBytesMap = CompressRamUtils.decompressByTar(ramBytes);
        System.out.println(fileBytesMap.size());
    }

	
	/**
     * 使用TAR算法进行压缩.
     * @param sourceFileBytesMap 待压缩文件的Map集合.
     * @return 压缩后的TAR文件字节数组.
     * @throws Exception 压缩过程中可能发生的异常，若发生异常，则返回的字节数组长度为0.
     */
    public static byte[] compressByTar(Map<String, byte[]> tarFileBytesMap) throws Exception {
        // 变量定义.
        ByteArrayOutputStream tarBaos = null;
        TarArchiveOutputStream tarTaos = null;
        TarArchiveEntry tarTae = null;

        try {
            // 压缩变量初始化.
            tarBaos = new ByteArrayOutputStream();
            tarTaos = new TarArchiveOutputStream(tarBaos);
            // // 将文件添加到TAR条目中.
            for (Map.Entry<String, byte[]> fileEntry : tarFileBytesMap.entrySet()) {
                tarTae = new TarArchiveEntry(fileEntry.getKey());
                tarTae.setName(fileEntry.getKey());
                tarTae.setSize(fileEntry.getValue().length);
                tarTaos.putArchiveEntry(tarTae);
                tarTaos.write(fileEntry.getValue());
                tarTaos.closeArchiveEntry();
            }
        } finally {
            if (tarTaos != null) {
                tarTaos.close();
            }
            if (null == tarBaos) {
                tarBaos = new ByteArrayOutputStream();
            }
        }
        return tarBaos.toByteArray();
    }

    /**
     * 使用TAR算法进行解压.
     * @param sourceZipFileBytes TAR文件字节数组.
     * @return 解压后的文件Map集合.
     * @throws Exception 解压过程中可能发生的异常，若发生异常，返回Map集合长度为0.
     */
    public static Map<String, byte[]> decompressByTar(byte[] sourceTarFileBytes) throws Exception {
        // 变量定义.
        TarArchiveEntry sourceTarTae = null;
        ByteArrayInputStream sourceTarBais = null;
        TarArchiveInputStream sourceTarTais = null;
        Map<String, byte[]> targetFilesFolderMap = null;

        try {
            // 解压变量初始化.
            targetFilesFolderMap = new HashMap<String, byte[]>();
            sourceTarBais = new ByteArrayInputStream(sourceTarFileBytes);
            sourceTarTais = new TarArchiveInputStream(sourceTarBais);
            // 条目解压缩至Map中.
            while ((sourceTarTae = sourceTarTais.getNextTarEntry()) != null) {
                targetFilesFolderMap.put(sourceTarTae.getName(), IOUtils.toByteArray(sourceTarTais));
            }
        } finally {
            if (sourceTarTais != null)
                sourceTarTais.close();
        }
        return targetFilesFolderMap;
    }

    /**
     * 使用GZIP算法进行压缩.
     * @param sourceFileBytesMap 待压缩文件的Map集合.
     * @return 压缩后的GZIP文件字节数组.
     * @throws Exception 压缩过程中可能发生的异常，若发生异常，则返回的字节数组长度为0.
     */
    public static byte[] compressByGZip(byte[] sourceFileBytes) throws IOException {
        // 变量定义.
        ByteArrayOutputStream gzipBaos = null;
        GzipCompressorOutputStream gzipGcos = null;

        try {
            // 压缩变量初始化.
            gzipBaos = new ByteArrayOutputStream();
            gzipGcos = new GzipCompressorOutputStream(gzipBaos);
            // 采用commons-compress提供的方式进行压缩.
            gzipGcos.write(sourceFileBytes);
        } finally {
            if (gzipGcos != null) {
                gzipGcos.close();
            }
            if (null == gzipBaos) {
                gzipBaos = new ByteArrayOutputStream();
            }
        }
        return gzipBaos.toByteArray();
    }

    /**
     * 使用GZIP算法进行解压.
     * @param sourceGZipFileBytes GZIP文件字节数组.
     * @return 解压后的文件Map集合.
     * @throws Exception 解压过程中可能发生的异常，若发生异常，则返回的字节数组长度为0.
     */
    public static byte[] decompressByGZip(byte[] sourceGZipFileBytes) throws IOException {
        // 变量定义.
        ByteArrayOutputStream gzipBaos = null;
        ByteArrayInputStream sourceGZipBais = null;
        GzipCompressorInputStream sourceGZipGcis = null;

        try {
            // 解压变量初始化.
            gzipBaos = new ByteArrayOutputStream();
            sourceGZipBais = new ByteArrayInputStream(sourceGZipFileBytes);
            sourceGZipGcis = new GzipCompressorInputStream(sourceGZipBais);
            // 采用commons-compress提供的方式进行解压.
            gzipBaos.write(IOUtils.toByteArray(sourceGZipGcis));
        } finally {
            if (sourceGZipGcis != null)
                sourceGZipGcis.close();
        }
        return gzipBaos.toByteArray();
    }



}
