package org.databandtech.mysql2hive.Bucket;

import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.apache.flink.streaming.api.functions.sink.filesystem.BucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.SimpleVersionedStringSerializer;

/**
 * 自定义的桶分配
 * @author Administrator
 *
 * @param <T>
 */
public class MemberBucketAssigner<T> implements BucketAssigner<T, String> {
    private static final long serialVersionUID = 1L;
 
	@Override
	public String getBucketId(T element, BucketAssigner.Context context) {
		return ""; 
	}

	@Override
	public SimpleVersionedSerializer<String> getSerializer() {
		return SimpleVersionedStringSerializer.INSTANCE;
	}

	@Override
	public String toString() {
		return "myCustemBucket";
	}
}
