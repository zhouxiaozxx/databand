import request from '@/utils/request'

// 查询生成表数据
export function listTable(query) {
  return request({
    url: '/tool/formgen/list',
    method: 'get',
    params: query
  })
}

// 导入表
export function importTable(data) {
    return request({
      url: '/tool/formgen/importTable',
      method: 'post',
      params: data
    })
  }

