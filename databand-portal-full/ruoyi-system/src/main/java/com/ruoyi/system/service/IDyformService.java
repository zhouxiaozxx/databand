package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Dyform;

/**
 * 动态单Service接口
 * 
 * @author ruoyi
 * @date 2021-06-11
 */
public interface IDyformService 
{
    /**
     * 查询动态单
     * 
     * @param id 动态单ID
     * @return 动态单
     */
    public Dyform selectDyformById(Long id);

    /**
     * 查询动态单列表
     * 
     * @param dyform 动态单
     * @return 动态单集合
     */
    public List<Dyform> selectDyformList(Dyform dyform);

    /**
     * 新增动态单
     * 
     * @param dyform 动态单
     * @return 结果
     */
    public int insertDyform(Dyform dyform);

    /**
     * 修改动态单
     * 
     * @param dyform 动态单
     * @return 结果
     */
    public int updateDyform(Dyform dyform);

    /**
     * 批量删除动态单
     * 
     * @param ids 需要删除的动态单ID
     * @return 结果
     */
    public int deleteDyformByIds(Long[] ids);

    /**
     * 删除动态单信息
     * 
     * @param id 动态单ID
     * @return 结果
     */
    public int deleteDyformById(Long id);
}
