package org.databandtech.job.entity;

import java.util.Arrays;

public class SaveMeta {

	public SaveMeta(String url, String username, String password, 
			String sqlForSave, String[] fieldNames,String[] keyField, Boolean isInsertOrUpdate) {
		super();
		this.fieldNames = fieldNames;
		this.keyField = keyField;
		this.isInsertOrUpdate = isInsertOrUpdate;
		this.sqlForSave = sqlForSave;
		this.url = url;
		this.username = username;
		this.password = password;
	}
	
    String sqlForSave;
	String url ;
    String username ;
	String password;
	String[] fieldNames; //用于保存查询后的业务数据字段
	String[] keyField; //联合主键，用于更新时定位逻辑
	Boolean isInsertOrUpdate; //true：代表要么新增，如果有原有记录则更新； false： 代表仅新增

	public String[] getKeyField() {
		return keyField;
	}
	public void setKeyField(String[] keyField) {
		this.keyField = keyField;
	}
	public Boolean getIsInsertOrUpdate() {
		return isInsertOrUpdate;
	}
	public void setIsInsertOrUpdate(Boolean isInsertOrUpdate) {
		this.isInsertOrUpdate = isInsertOrUpdate;
	}
	public String getSql() {
		return sqlForSave;
	}
	public void setSql(String sqlForSave) {
		this.sqlForSave = sqlForSave;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String[] getFieldNames() {
		return fieldNames;
	}
	public void setFieldNames(String[] fieldNames) {
		this.fieldNames = fieldNames;
	}
	
}
