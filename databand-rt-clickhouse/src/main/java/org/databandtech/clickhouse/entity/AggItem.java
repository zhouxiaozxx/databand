package org.databandtech.clickhouse.entity;

import java.util.Map;

public class AggItem {
	
	String source;
	String datekey;
	String pt;
	Map<String , Integer> valueMap;

	public AggItem(String source, String datekey, String pt) {
		super();
		this.source = source;
		this.datekey = datekey;
		this.pt = pt;
	}
	
	public void addMap(Map<String , Integer> valueMap) {
		this.valueMap = valueMap;
	}
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDatekey() {
		return datekey;
	}
	public void setDatekey(String datekey) {
		this.datekey = datekey;
	}
	public String getPt() {
		return pt;
	}
	public void setPt(String pt) {
		this.pt = pt;
	}
	public Map<String, Integer> getValueMap() {
		return valueMap;
	}
	public void setValueMap(Map<String, Integer> valueMap) {
		this.valueMap = valueMap;
	}

	@Override
	public String toString() {
		return "AggItem [source=" + source + ", datekey=" + datekey + ", pt=" + pt + ", valueMap=" + valueMap + "]";
	}

	

}
